import func
import parsing


def main():
    ans, n = func.alg(*parsing.pars())
    for i in range(n):
        print(f"{i}) {ans[i % 3]} = {str(bin(ans[i % 3]))[2::]}")


if __name__ == "__main__":
    main()
