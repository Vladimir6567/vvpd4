import argparse


def pars():
    parser = argparse.ArgumentParser()
    parser.add_argument("--a", type=int, default=3,
                        help="Первый член последовательности", )
    parser.add_argument("--b", type=int, default=5,
                        help="Второй член последовательности", )
    parser.add_argument("--n", type=int, default=11,
                        help="Длина последовательности", )

    cmd = parser.parse_args()
    a, b, n = cmd.a, cmd.b, cmd.n
    return a, b, n
