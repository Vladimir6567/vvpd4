def check(a, b, c):
    return a > 0 and b > 0 and c > 0


def alg(a, b, n):
    if check(a, b, n):
        c = a ^ b
        ans = (a, b, c)
        return ans, n
    else:
        print("Не коректно введённые данные!")
        return None


def test_check():
    assert check(10, 10, 10) is True
    assert check(-10, 10, 10) is False
    assert check(10, -10, 10) is False
    assert check(10, 10, -10) is False
    assert check(-10, -10, -10) is False


def test_als():
    assert alg(3, 4, 2) == ((3, 4, 7), 2)
    assert alg(3, -4, 2) is None

